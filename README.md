***This is no longer the repo for NoMAD***

The NoMAD repo has moved to [github.com](https://github.com/jamf/NoMAD). All support and development of NoMAD is now being done by Jamf. This archive will eventually go away, handle your affairs accordingly.
